# Terms and Conditions

Fourth FTP Writer component for KBC is offered as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to send data from Keboola Connection Platform (KBC) storage to FTP. 
