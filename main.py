"__author__ = 'Martin Fiser @VFisa'"
"__credits__ = 'Original code by Leo Chan. Keboola 2017'"

"""
Python 3 environment (unicode script fixes in place)
"""


import csv
import ftplib
import logging
import math
import os
import sys
import pandas as pd
from keboola import docker
import logging_gelf.formatters
import logging_gelf.handlers
import paramiko


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
logger = logging.getLogger()
logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
    host=os.getenv('KBC_LOGGER_ADDR'),
    port=int(os.getenv('KBC_LOGGER_PORT'))
    )
logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
logger.addHandler(logging_gelf_handler)

# removes the initial stdout logging
logger.removeHandler(logger.handlers[0])

# get proper list of tables
DEFAULT_FILE_DESTINATION = "/data/in/tables/"
DEFAULT_OUTPUT_DESTINATION = "/data/"

"""
Configuration
{
    "sftp_url":"sftp.stg-us-east.medispend.com",
    "destination":"/outbound/",
    "user":"kbc",
    "#password":"",
    "enclosure": "YES"
}
"""

# Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
SFTP_URL = cfg.get_parameters()["sftp_url"]
DESTINATION = cfg.get_parameters()["destination"]
USER = cfg.get_parameters()["user"]
PASSWORD = cfg.get_parameters()["#password"]
ENCLOSURE = cfg.get_parameters()["enclosure"]

# Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
logging.debug("IN tables mapped: "+str(in_tables))

# Setup enclosure quote
if ENCLOSURE=="YES":
    QUOTE = csv.QUOTE_MINIMAL
elif ENCLOSURE=="NO":
    QUOTE = csv.QUOTE_NONE
else:
    logging.warning("Quote parameter is wrong, ignoring.")
    QUOTE = csv.QUOTE_MINIMAL


def get_tables(in_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.debug("Data table: " + str(in_name))
    logging.debug("Input table source: " + str(in_destination))

    return in_name, in_destination



if __name__ == "__main__":
    """
    Main execution script.
    """
    
    ## SFTP Connection
    
    try:
        ftp_connection = ftplib.FTP(SFTP_URL, USER, PASSWORD)
        remote_path = DESTINATION
        ftp_connection.cwd(remote_path)
        logging.info("Connected.")
    except Exception as e:
        logging.error("Could not log in! Check credentials!")
        logging.error(str(e))
        sys.exit(1)

    # Read file in Pandas
    in_file,in_file_name = get_tables(in_tables)
    logging.debug("File Source: " + in_file)
    logging.info("File Name: "+in_file_name)
    data_df = pd.read_csv(in_file, dtype=str)
    #print(data_df)
    
    # Output data in desired format
    data_df.to_csv(DEFAULT_OUTPUT_DESTINATION+in_file_name, index=False, quoting=QUOTE)

    # Upload file
    source = (DEFAULT_OUTPUT_DESTINATION+in_file_name)
    destination = (str(DESTINATION)+in_file_name)
    try:
        command = ("STOR "+str(in_file_name))
        #print(command)
        fh = open(source, 'rb')
        ftp_connection.storbinary(command, fh)
        fh.close()
        logging.debug("File Output Source: " + source)
        logging.info("File Output Destination: "+destination)
    except Exception as e:
        logging.error("Could not upload file to FTP! Exit.")
        logging.error(str(e))
        sys.exit(1)
    
    # Finish
    logging.info("Script completed.")
